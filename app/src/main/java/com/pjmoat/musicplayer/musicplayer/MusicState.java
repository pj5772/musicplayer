package com.pjmoat.musicplayer.musicplayer;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import java.io.IOException;
import java.util.ArrayList;

public class MusicState {
    private final String TAG = "com.pjmoat.musicplayer";

    //currently playing song id
    public long current_song_id = -1;

    //this variable holds the point the user was in the song(ranges from 0 to 100)
    public int last_play_time = 0;

    //holds position of currently swiped element
    public int current_view_swiped_pos = -1;

    //size of button to open second activity (setup in MainActivity)
    public int view_song_button_width = 10;

    //accessor for single instance of class
    private static final MusicState musicState = new MusicState();
    public static MusicState getInstance() {return musicState;}

    //holds all songs on device
    public ArrayList<Song> songs = new ArrayList<>();

    //mediaplayer that handles audio
    public MediaPlayer mMediaPlayer;

    @Override
    public String toString() {
        String msg = "current_song_id: " + current_song_id +
                     ", last_play_time: " + last_play_time +
                     ", current_view_swiped_pos: " + current_view_swiped_pos;
        return msg;
    }
}
