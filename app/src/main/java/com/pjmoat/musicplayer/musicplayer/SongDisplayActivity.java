package com.pjmoat.musicplayer.musicplayer;

import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class SongDisplayActivity extends AppCompatActivity {

    private final String TAG = "com.pjmoat.musicplayer";

    Bitmap pausebuttonimage, playbuttonimage;

    Song current_song;

    boolean showing_play_button = false;

    //handlers for worker_thread
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    Thread worker_thread = new Thread(new Runnable() {
        @Override
        public void run() {
            synchronized (this) {
                //update the circular progressbar
                updateProgressBar();

                //if song is finished, and we are not showing the play button fix that
                if (!showing_play_button && songFinished()) {
                    updatePlayPauseButton();
                }
            }

            mHandler.postDelayed(this, 1000);
        }
    });

    //handle if user hits back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_display);

        //enable back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //get rid of text
        getSupportActionBar().setTitle("");

        //make the actionbar correct color
        int bg_color = ContextCompat.getColor(this, R.color.activityBgColor);;
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(bg_color));

        //remove outline at bottom of actionbar
        getSupportActionBar().setElevation(0);

        //load song from intent
        int current_song_index = Integer.parseInt(getIntent().getStringExtra("current_song_index"));
        current_song = MusicState.getInstance().songs.get(current_song_index);

        //setup play and pause button bitmaps
        Bitmap pausebuttonimage_unscaled = BitmapFactory.decodeResource(getResources(), R.drawable.pausebutton);
        pausebuttonimage = Bitmap.createScaledBitmap(pausebuttonimage_unscaled, 50, 50, false);
        Bitmap playbuttonimage_unscaled = BitmapFactory.decodeResource(getResources(), R.drawable.playbutton);
        playbuttonimage = Bitmap.createScaledBitmap(playbuttonimage_unscaled, 50, 50, false);

        //start thread to manage progressbar
        worker_thread.start();

        //setup album cover CircleImageView to currently swiped song's album cover
        CircleImageView albumView = (CircleImageView) findViewById(R.id.albumcover_circleimageview);
        albumView.setImageBitmap(current_song.album_art);

        //setup play/pause button to be accurate
        updatePlayPauseButton();
    }

    //handle when user presses pause/play button
    public void onPlayPauseClicked(View v) {

        //are we currently playing music
        if (isPlayingMusic())
            pauseSong(); //if we are pause
        else {

            //were we playing music before?
            if (MusicState.getInstance().mMediaPlayer == null)
                playSong(current_song); //no music was playing, start the song we are currently displaying
            else
                resumeSong(); //music was playing, resume it
        }

        //update button above album cover with play/pause
        updatePlayPauseButton();
    }

    //handle fastforward button clicked
    public void onFastForwardClicked(View v) {
        //set currentl playing song to this song
        MusicState.getInstance().current_song_id = current_song.id;

        //find new progress
        int new_progress = currentSongTime() + ((int) (100.0*(5000.0/((double) current_song.duration))));

        //make sure it is less than 100%
        new_progress = Math.min(100,new_progress);

        //move song to the new progress
        seekTo(new_progress);

        //update button, now that we possible went from playing to pausing
        updatePlayPauseButton();
    }

    //handle rewind button clicked
    public void onRewindClicked(View v) {
        //set currentl playing song to this song
        MusicState.getInstance().current_song_id = current_song.id;

        //find new progress
        int new_progress = currentSongTime() - ((int) (100.0*(5000.0/((double) current_song.duration))));

        //make sure it is less than 100%
        new_progress = Math.max(0,new_progress);

        //move song to the new progress
        seekTo(new_progress);

        //update button, now that we possible went from playing to pausing
        updatePlayPauseButton();
    }

    //seeks to progress in currently playing song
    public void seekTo(int progress) {
        //set song playing to the current song that is open
        MusicState.getInstance().current_song_id = current_song.id;

        //update last play time to the new place we want to start song
        MusicState.getInstance().last_play_time = progress;

        //start song at new play time
        resumeSong();
    }

    //update progress bar with current progress in song
    private void updateProgressBar() {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        if (progressBar != null)
            progressBar.setProgress(currentSongTime()*10);
    }

    //updates play/pause icon depending on if music is playing
    private void updatePlayPauseButton() {
        ImageView playpauseView = (ImageView) findViewById(R.id.playpause_imageview);

        if (isPlayingMusic()) {
            playpauseView.setImageBitmap(pausebuttonimage);
            showing_play_button = false;
        }
        else {
            playpauseView.setImageBitmap(playbuttonimage);
            showing_play_button = true;
        }
    }

    //returns whether app is currently playing music
    public boolean isPlayingMusic() {
        if (MusicState.getInstance().mMediaPlayer != null)
            return MusicState.getInstance().mMediaPlayer.isPlaying();
        else
            return false;
    }

    //plays song
    public void playSong(Song s) {
        Log.d(TAG,"playing song: " + s.toString());

        //reset last_play_time
        MusicState.getInstance().last_play_time = 0;

        //set currently playing song id
        MusicState.getInstance().current_song_id = s.id;

        try {
            //create media player
            MusicState.getInstance().mMediaPlayer = new MediaPlayer();
            MusicState.getInstance().mMediaPlayer.setDataSource(s.path);

            //code to handle when song finishes
            MusicState.getInstance().mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    MusicState.getInstance().current_song_id = -1;
                    stopSong();
//                    updateIconsBg();
                }
            });

            //start playing the song
            MusicState.getInstance().mMediaPlayer.prepare();
            MusicState.getInstance().mMediaPlayer.start();
        }
        catch (IOException e) {
            Log.e(TAG, "error occured while playing song...");
        }
    }

    //pauses song and saved current time in song
    public void pauseSong() {
        Log.d(TAG,"pausing song...");

        if (MusicState.getInstance().mMediaPlayer != null) {
            MusicState.getInstance().mMediaPlayer.pause();
            MusicState.getInstance().last_play_time = currentSongTime();
        }
    }

    //resumes song where you last left off
    public void resumeSong() {
        Log.d(TAG,"resuming song...");

        //if mediaplayer is not configured
        if (MusicState.getInstance().mMediaPlayer == null) {
            //get current song
            Song current_song = getSongWithID(MusicState.getInstance().current_song_id);

            //no song is selected, so there is nothing to resume. exit
            if (current_song == null)
                return;

            try {
                //create media player
                MusicState.getInstance().mMediaPlayer = new MediaPlayer();
                MusicState.getInstance().mMediaPlayer.setDataSource(current_song.path);

                //code to handle when song finishes
                MusicState.getInstance().mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        MusicState.getInstance().current_song_id = -1;
                        stopSong();
                        //updateIconsBg();
                    }
                });
                MusicState.getInstance().mMediaPlayer.prepare();
                MusicState.getInstance().mMediaPlayer.start();
                MusicState.getInstance().mMediaPlayer.pause();
            }
            catch (IOException e) {
                Log.e(TAG, "error occured while playing song...");
            }
        }

        //seek to point where user last pressed pause
        MusicState.getInstance().mMediaPlayer.seekTo(MusicState.getInstance().last_play_time * 1000);
        MusicState.getInstance().mMediaPlayer.start();
    }

    //returns song with given id
    private Song getSongWithID(long id) {
        for (Song song: MusicState.getInstance().songs) {
            if (song.id == id)
                return song;
        }

        return null;
    }

    //stops any currently playing song
    public void stopSong() {
        Log.d(TAG,"stopping song..");

        MusicState.getInstance().last_play_time = 0;
        MusicState.getInstance().current_song_id = -1;

        if (MusicState.getInstance().mMediaPlayer != null) {
            MusicState.getInstance().mMediaPlayer.stop();
            MusicState.getInstance().mMediaPlayer.release();
            MusicState.getInstance().mMediaPlayer = null;
        }
    }

    private boolean songFinished() {
        if (MusicState.getInstance().mMediaPlayer == null)
            return true;
        else
            return (currentSongTime() > 100);
    }

    //returns how far you are into song using percent 0 to 100
    private synchronized int currentSongTime() {
        if ((MusicState.getInstance().mMediaPlayer != null) && (MusicState.getInstance().current_song_id != -1))
            return (MusicState.getInstance().mMediaPlayer.getCurrentPosition() / 1000);
        else
            return 0;
    }
}
