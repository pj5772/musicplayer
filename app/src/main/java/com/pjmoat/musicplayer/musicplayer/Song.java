package com.pjmoat.musicplayer.musicplayer;

import android.graphics.Bitmap;

public class Song {
    long id, album_id;
    String artist,title,path;
    int duration; //ms
    Bitmap album_art;

    public Song(long id, String artist, String title, String path, int duration, long album_id, Bitmap album_art) {
        this.id = id;
        this.artist = artist;
        this.title = title;
        this.path = path;
        this.duration = duration;
        this.album_id = album_id;
        this.album_art = album_art;
    }

    @Override
    public String toString() {
        return "(" + id + "," + artist + "," + title +  ", " + album_id +")";
    }
}
