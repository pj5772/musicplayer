package com.pjmoat.musicplayer.musicplayer;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "com.pjmoat.musicplayer";

    //button used to view song in its own activity
    private ImageView viewSongButton;

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;

    //songs list view
    RecyclerView songs_view;

    //songs adapter
    private SongsAdapter songsAdapter;

    RecyclerView.LayoutManager mLayoutManager;

    private boolean view_song_button_visible = false;

    //handlers for worker_thread
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    //create swipe controller
    SwipeController swipeController;

    private Bitmap playbuttonimage;
    private Bitmap pausebuttonimage;

    Thread worker_thread = new Thread(new Runnable() {
        @Override
        public void run() {
            synchronized (this) {
                //once if the viewsong imageview is not yet created and songs_view is
                //created, set up viewsong_imageview
                if ((!view_song_button_visible) &&(songs_view.getLayoutManager().findViewByPosition(0) != null)) {
                    createViewSongButton();
                    view_song_button_visible = true;
                }

                //updates all progress bar with new progress
                updateProgressBars();

                //update time above currently playing song
                updateSongTime();

                //stop song if it is finished
                if (songFinished()) {
                    stopSong();
                    updateIconsBg();
                }

                //update icons and background
                updateIconsBg();
            }

            mHandler.postDelayed(this, 1000);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hide the actionbar
        getSupportActionBar().hide();

        //if we don't have permission, get it
        if (!havePermission()) {
            getPermission();
            return;
        }

        //we have permission so load songs and create songs recycler view
        else {
            loadSongs();
            createSongView();
        }

        worker_thread.start();

        //create pause button and play button bitmaps
        Bitmap pausebuttonimage_unscaled = BitmapFactory.decodeResource(getResources(), R.drawable.pausebutton);
        pausebuttonimage = Bitmap.createScaledBitmap(pausebuttonimage_unscaled, 50, 50, false);
        Bitmap playbuttonimage_unscaled = BitmapFactory.decodeResource(getResources(), R.drawable.playbutton);
        playbuttonimage = Bitmap.createScaledBitmap(playbuttonimage_unscaled, 50, 50, false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //set last_play_time to current time in song
        MusicState.getInstance().last_play_time = currentSongTime();

        //store state
        outState.putLong("current_song_id",MusicState.getInstance().current_song_id);
        outState.putInt("last_play_time", MusicState.getInstance().last_play_time);
        if (MusicState.getInstance().mMediaPlayer == null)
            outState.putBoolean("is_paused",true);
        else
            outState.putBoolean("is_paused",!MusicState.getInstance().mMediaPlayer.isPlaying());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        //set to restored state
        MusicState.getInstance().current_song_id = savedInstanceState.getLong("current_song_id");
        MusicState.getInstance().last_play_time = savedInstanceState.getInt("last_play_time");

        //resume song where it was left off
        if (savedInstanceState.getBoolean("is_paused")) {
            resumeSong();
            pauseSong();
        }
        else
            resumeSong();

        //update icons and background
        updateIconsBg();

        super.onRestoreInstanceState(savedInstanceState);
    }

    //opens activity to view song
    private void openSongView() {
        //store current time in song
        MusicState.getInstance().last_play_time = currentSongTime();

        //if we have a song currently swiped
        if (MusicState.getInstance().current_view_swiped_pos != -1) {

            //start new activity to view song
            Intent i = new Intent(MainActivity.this, SongDisplayActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.putExtra("current_song_index", Integer.toString(MusicState.getInstance().current_view_swiped_pos));
            startActivity(i);
        }

        //unswiped swiped song sincwe we have now openned a new activity to voew the song
        MusicState.getInstance().current_view_swiped_pos = -1;
    }

    //updates time above seekbar
    private void updateSongTime() {
        //find row of selected song
        for (int i = 0; i < MusicState.getInstance().songs.size(); i++) {
            //holds view of given row
            View rowView;
            rowView = songs_view.getChildAt(i);

            if (rowView == null)
                continue;

            //reference to seekbar of first row
            SeekBar seekbar = (SeekBar) rowView.findViewById(R.id.song_seekbar);

            //reference to time in selected row
            TextView timeTextView = (TextView) rowView.findViewById(R.id.time_textview);

            //if this is the selected element
            if (MusicState.getInstance().songs.get(i).id == MusicState.getInstance().current_song_id) {

                //get current progress of this seekbar
                double progress = ((double) seekbar.getProgress()) / ((double) seekbar.getMax());

                //find x cooridinate of time label textview
                int x = ((int) ((seekbar.getWidth()-4*seekbar.getThumbOffset())*progress)) + seekbar.getLeft();
                x += + seekbar.getThumbOffset() / 2;
                //current time of song in ms
                int t_ms = (int) ((currentSongTime()/100.0) * MusicState.getInstance().songs.get(i).duration);

                //current time into song in minutes and seconds
                int mins = (int) Math.floor((1.0/60000.0) * t_ms);
                int secs = (int) Math.floor((t_ms % 60000.0)/1000.0);

                //set the text
                if (secs < 10)
                    timeTextView.setText(mins + ":0" + secs); //Ex: 4:5 -> 4:05
                else
                    timeTextView.setText(mins + ":" + secs); //Ex: 4:32

                //set location of this textview to follow seekbar
                timeTextView.setX(x);

                //make this text view visible
                timeTextView.setVisibility(View.VISIBLE);
            }

            else {
                //move textview to the left end of seekbar
                timeTextView.setX(seekbar.getLeft());

                //we don't want to see this time textview since this song is not playing
                timeTextView.setVisibility(View.INVISIBLE);
            }

        }
    }

    //updates play pause icon
    private void updateIconsBg() {
        View rowView;
        //loop over rows
        for (int i = 0 ; i < MusicState.getInstance().songs.size(); i++) {
            //holds view of given row
            rowView = songs_view.getChildAt(i);

            if (rowView == null)
                continue;

            //stores imageview of icon
            ImageView img = rowView.findViewById(R.id.play_pause_imageview);

            //get refence to textview for song title and time above progress
            TextView title_textview = (TextView) rowView.findViewById(R.id.title_textview);
            TextView time_textview  = (TextView) rowView.findViewById(R.id.time_textview);

            //if this is song we are playing, and we are currently playing music
            if (MusicState.getInstance().songs.get(i).id == MusicState.getInstance().current_song_id) {

                //make affected text white
                title_textview.setTextColor(Color.WHITE);
                time_textview.setTextColor(Color.WHITE);

                //set background color of selected song
                rowView.setBackgroundColor(Color.parseColor("#FFF02F5C"));

                //display pause/play view on this view because it is the selected song row
                img.setVisibility(View.VISIBLE);

                //set play/pause based on if currently playing music
                if (isPlayingMusic())
                    img.setImageBitmap(pausebuttonimage);
                else
                    img.setImageBitmap(playbuttonimage);
            }

            //non-selected rows
            else {
                //make play/pause imageview invisible
                img.setVisibility(View.INVISIBLE);

                //make affected text white
                title_textview.setTextColor(Color.BLACK);
                time_textview.setTextColor(Color.BLACK);

                //not selected row so make white
                rowView.setBackgroundColor(Color.WHITE);
            }
        }
    }

    private void updateProgressBars() {
        View rowView;
        //loop over rows
        for (int i = 0 ; i < MusicState.getInstance().songs.size(); i++) {
            //holds view of given row
            rowView = songs_view.getChildAt(i);

            if (rowView == null)
                continue;

            //stores progressbar of this row of icon
            SeekBar song_seekbar = (SeekBar) rowView.findViewById(R.id.song_seekbar);

            //if this is song we are playing, and we are currently playing music
            if ((MusicState.getInstance().songs.get(i).id == MusicState.getInstance().current_song_id)) {

                //make seek bar visible
                song_seekbar.setVisibility(View.VISIBLE);

                //set progressbar
                song_seekbar.setProgress(currentSongTime());

            }

            //this song is not selected, reset seekbar and make invisible
            else {
                song_seekbar.setProgress(0);
                song_seekbar.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //if this is READ_EXTERNAL_STORAGE permission
        if (permissions[0].equals("android.permission.READ_EXTERNAL_STORAGE")) {

            //if permission was granted
            if (grantResults[0] == 0) {

                //restart app, now with permissions
                Intent i = getBaseContext().getPackageManager()
                        .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(i);
            }
        }
    }

    //returns whether we have READ_EXTERNAL_STORAGE permission
    private boolean havePermission() {
        return (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED);
    }

    private void getPermission() {
        //ask for permission if app does not already have permission
        if (!havePermission()) {

            //request permission
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }

    }

    private Bitmap getAlbumArt(long album_id) {
        //content provider to retreive album art
        Cursor cursor = getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[] {MediaStore.Audio.Albums._ID, MediaStore.Audio.Albums.ALBUM_ART},
                MediaStore.Audio.Albums._ID+ "=?",
                new String[] {String.valueOf(album_id)},
                null);

        //will hold path to album art
        String album_art_path;

        //if there is album art associated with id
        if (cursor.moveToFirst()) {

            //set path
            album_art_path = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
        }
        //could not find any album art associated with this id
        else
            return null;

        //store album art as bitmap
        Bitmap album_art = BitmapFactory.decodeFile(album_art_path);

        return album_art;
    }

    //loads all songs from device
    private void loadSongs() {

        // First create a ContentResolver instance, retrieve the URI for external
        // music files, and create a Cursor instance using the ContentResolver
        // instance to query the music files:
        ContentResolver musicResolver = getContentResolver();
        Uri musicUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null);

        //Now you can iterate over the results:
        if(musicCursor!=null && musicCursor.moveToFirst()){
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ARTIST);
            int pathColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.DATA);
            int albumIDColumn = musicCursor.getColumnIndex
                    (android.provider.MediaStore.Audio.Media.ALBUM_ID);

            //add songs to list
            do {
                //get song data
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                String path = musicCursor.getString(pathColumn);
                long thisAlbumId = musicCursor.getLong(albumIDColumn);

                //get duration of song
                MediaPlayer mp = MediaPlayer.create(this, Uri.parse(path));
                int duration = mp.getDuration();

                //get album art as bitmap
                Bitmap album_art = getAlbumArt(thisAlbumId);

                //if cannot find album art, use default
                if (album_art == null)
                    album_art = BitmapFactory.decodeResource(this.getResources(),
                                    R.drawable.default_album_cover);

                //add this song to list of songs
                MusicState.getInstance().songs.add(new Song(thisId,thisArtist,thisTitle,path,duration,thisAlbumId,album_art));
            }
            while (musicCursor.moveToNext());
        }
    }

    private void createSongView() {
        //if already created, don't recreate
        if (songs_view != null)
            return;

        //set adapter of listview
        //creating adapter
        songs_view = (RecyclerView) findViewById(R.id.songs_list);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        songs_view.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        songs_view.setLayoutManager(mLayoutManager);

        // specify an adapter for recyclerview
        songsAdapter = new SongsAdapter(this, R.layout.row, MusicState.getInstance().songs, new SongsAdapter.SeekbarViewProgressChangedListener() {
            @Override
            public void onSeekBarProgressChanged(int progress,boolean fromUser) {
                Log.d(TAG,"<onSeekbarProgressChanged> progress: " + progress + ", fromUser: " + fromUser);

                //setup last_play_time with new value
                MusicState.getInstance().last_play_time = progress;

                if (fromUser) {
                    //set cursor
                    if (MusicState.getInstance().mMediaPlayer != null)
                        MusicState.getInstance().mMediaPlayer.seekTo(MusicState.getInstance().last_play_time * 1000);

                    //if mediaplayer is already defined, hop to this value
                    if (isPlayingMusic())
                        resumeSong();
                }

                //update progressbar
                updateProgressBars();

                //update textview above seekbar
                updateSongTime();
            }
        });
        songs_view.setAdapter(songsAdapter);

        songs_view.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, songs_view ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onLongItemClick(View view, int position) {}

                    @Override
                    public void onItemClick(View view, int position) {
                        //if this is the song currently being played
                        if (MusicState.getInstance().current_song_id == MusicState.getInstance().songs.get(position).id) {

                            //song is already playing so either pause or resume
                            if (isPlayingMusic())
                                pauseSong();
                            else
                                resumeSong();
                        }
                        else {
                            //stop any currently playing song
                            stopSong();

                            //play song at this position
                            playSong(MusicState.getInstance().songs.get(position));
                        }

                        //make sure icons in listview are correct
                        updateIconsBg();

                        //update progress bars
                        updateProgressBars();
                    }
                })
        );

        //if user clicks swiped button on song, open a new songview
        swipeController = new SwipeController(new SwipeController.OnSongButtonClickListener() {
            @Override
            public void onOpenSongButtonClicked() {
                openSongView();
            }

            @Override
            public void onSeekBarProgressChanged(int progress) {
                //setup last_play_time with new value
                MusicState.getInstance().last_play_time = progress;

                //set cursor
                if (MusicState.getInstance().mMediaPlayer != null)
                    MusicState.getInstance().mMediaPlayer.seekTo(MusicState.getInstance().last_play_time * 1000);

                //if mediaplayer is already defined, hop to this value
                if (isPlayingMusic())
                    resumeSong();
            }
        });

        //attach itemTouchHelper to handle swiping
        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(songs_view);
    }

    private void createViewSongButton() {
        //find height of songs_view (we must measure it first)
//        songs_view.measure(
//                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
//                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
//        int song_view_height = songs_view.getMeasuredHeight();

        //setup view song button
        viewSongButton =  (ImageView) findViewById(R.id.viewSongImageView);

        //holds size of one element
        MusicState.getInstance().view_song_button_width = songs_view.getLayoutManager().findViewByPosition(0).getHeight();

        //set button image
        Bitmap viewsongimage_unscaled = BitmapFactory.decodeResource(getResources(), R.drawable.viewsong_icon);
        Bitmap viewsongimage = Bitmap.createScaledBitmap(viewsongimage_unscaled,
                MusicState.getInstance().view_song_button_width,
                MusicState.getInstance().view_song_button_width,
                false);

        //setup bitmap that will hold all buttons concatenated
        Bitmap.Config conf = Bitmap.Config.ARGB_8888;
        Bitmap all_buttons_bitmap = Bitmap.createBitmap(MusicState.getInstance().view_song_button_width,
                MusicState.getInstance().songs.size() * MusicState.getInstance().view_song_button_width,
                                                        conf);
        Canvas newCanvas = new Canvas(all_buttons_bitmap);

        //add button images to all_buttons_bitmap top to bottom
        for (int i = 0; i < MusicState.getInstance().songs.size(); i++) {
            newCanvas.drawBitmap(viewsongimage, 0, i * MusicState.getInstance().view_song_button_width, null);
        }

        //set viewSongButton's image to the image of concatenated buttons
        viewSongButton.setImageBitmap(all_buttons_bitmap);

        //move combined buttons to the corrent position
        viewSongButton.setX(Resources.getSystem().getDisplayMetrics().widthPixels -
                MusicState.getInstance().view_song_button_width);
        viewSongButton.setY(0);

        //setup Layout params for viewSongButton
        ViewGroup.LayoutParams params = viewSongButton.getLayoutParams();
        params.height = MusicState.getInstance().view_song_button_width * MusicState.getInstance().songs.size();
        params.width = MusicState.getInstance().view_song_button_width;
        viewSongButton.setLayoutParams(params);
    }

    //this method will be called if song recyclerView not yet created
    public void onScreenClicked(View v) {
          if (!havePermission())
              getPermission();
    }

    //returns whether app is currently playing music
    public boolean isPlayingMusic() {
        if (MusicState.getInstance().mMediaPlayer != null)
            return MusicState.getInstance().mMediaPlayer.isPlaying();
        else
            return false;
    }

    //plays song
    public void playSong(Song s) {
        Log.d(TAG,"playing song: " + s.toString());

        //reset last_play_time
        MusicState.getInstance().last_play_time = 0;

        //set currently playing song id
        MusicState.getInstance().current_song_id = s.id;

        try {
            //create media player
            MusicState.getInstance().mMediaPlayer = new MediaPlayer();
            MusicState.getInstance().mMediaPlayer.setDataSource(s.path);

            //code to handle when song finishes
            MusicState.getInstance().mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    MusicState.getInstance().current_song_id = -1;
                    stopSong();
//                    updateIconsBg();
                }
            });

            //start playing
            MusicState.getInstance().mMediaPlayer.prepare();
            MusicState.getInstance().mMediaPlayer.start();
        }
        catch (IOException e) {
            Log.e(TAG, "error occured while playing song...");
        }
    }

    //pauses song and saved current time in song
    public void pauseSong() {
        Log.d(TAG,"pausing song...");

        if (MusicState.getInstance().mMediaPlayer != null) {
            MusicState.getInstance().mMediaPlayer.pause();
            MusicState.getInstance().last_play_time = currentSongTime();
        }
    }

    //resumes song where you last left off
    public void resumeSong() {
        Log.d(TAG,"resuming song...");

        //if mediaplayer is not configured
        if (MusicState.getInstance().mMediaPlayer == null) {
            //get current song
            Song current_song = getSongWithID(MusicState.getInstance().current_song_id);

            //no song is selected, so there is nothing to resume. exit
            if (current_song == null)
                return;

            try {
                //create media player
                MusicState.getInstance().mMediaPlayer = new MediaPlayer();
                MusicState.getInstance().mMediaPlayer.setDataSource(current_song.path);

                //code to handle when song finishes
                MusicState.getInstance().mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        MusicState.getInstance().current_song_id = -1;
                        stopSong();
                        //updateIconsBg();
                    }
                });
                MusicState.getInstance().mMediaPlayer.prepare();
                MusicState.getInstance().mMediaPlayer.start();
                MusicState.getInstance().mMediaPlayer.pause();
            }
            catch (IOException e) {
                Log.e(TAG, "error occured while playing song...");
            }
        }

        //seek to point where user last pressed pause
        MusicState.getInstance().mMediaPlayer.seekTo(MusicState.getInstance().last_play_time * 1000);
        MusicState.getInstance().mMediaPlayer.start();
    }

    //returns song with given id
    private Song getSongWithID(long id) {
        for (Song song: MusicState.getInstance().songs) {
            if (song.id == id)
                return song;
        }

        return null;
    }

    //stops any currently playing song
    public void stopSong() {
        Log.d(TAG,"stopping song..");

        MusicState.getInstance().last_play_time = 0;
        MusicState.getInstance().current_song_id = -1;

        if (MusicState.getInstance().mMediaPlayer != null) {
            MusicState.getInstance().mMediaPlayer.stop();
            MusicState.getInstance().mMediaPlayer.release();
            MusicState.getInstance().mMediaPlayer = null;
        }
    }

    //returns whether song is still playing
    private boolean songFinished() {
        return (currentSongTime() > 100);
    }

    //returns how far you are into song using percent 0 to 100
    private synchronized int currentSongTime() {
        if ((MusicState.getInstance().mMediaPlayer != null) && (MusicState.getInstance().current_song_id != -1))
            return (MusicState.getInstance().mMediaPlayer.getCurrentPosition() / 1000);
        else
            return 0;
    }

}

