package com.pjmoat.musicplayer.musicplayer;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.awt.font.TextAttribute;

import de.hdodenhof.circleimageview.CircleImageView;

public class SongHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final String TAG = "com.pjmoat.musicplayer";

    private final CircleImageView song_imageview;
    private final ImageView playpause_imageview;
    private final TextView title_textview;
    private final TextView artist_textview;
    private final TextView duration_textview;
    private final TextView time_textview;
    private final SeekBar seekbar;

    private Song song;
    private Context context;

    SeekbarProgressChangedListener mListener;
    interface SeekbarProgressChangedListener {
        void onSeekBarProgressChanged(int new_progress,boolean fromUser);
    }

    public SongHolder(Context context, View itemView,SeekbarProgressChangedListener listener) {

        super(itemView);

        mListener = listener;

        this.context = context;

        // set up the UI widgets of the holder
        this.song_imageview = (CircleImageView) itemView.findViewById(R.id.song_imageview);
        this.playpause_imageview = (ImageView) itemView.findViewById(R.id.play_pause_imageview);
        this.title_textview = (TextView) itemView.findViewById(R.id.title_textview);
        this.artist_textview = (TextView) itemView.findViewById(R.id.artist_textview);
        this.duration_textview = (TextView) itemView.findViewById(R.id.duration_textview);
        this.time_textview = (TextView) itemView.findViewById(R.id.time_textview);
        this.seekbar = (SeekBar) itemView.findViewById(R.id.song_seekbar);

        // set the onClick listener of the holder
        itemView.setOnClickListener(this);
    }

    //binds a particular song to this song holder
    public void bindSong(Song song) {
        //set artist textview
        this.artist_textview.setText(song.artist);

        //set title textview
        this.title_textview.setText(song.title);

        //setup imageView
        Bitmap album_art_scaled = Bitmap.createScaledBitmap(song.album_art, 150, 150, false);
        song_imageview.setImageBitmap(album_art_scaled);

        this.playpause_imageview.setVisibility(View.INVISIBLE);

        //setup seeekbar onclick listener
        this.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //pass up that the progress of progressbar changed
                mListener.onSeekBarProgressChanged(progress,fromUser);
            }
        });

        //time in ms
        double t_ms = song.duration;

        //minutes and seconds
        int mins = (int) Math.floor((1.0/60000.0) * t_ms);
        int secs = (int) Math.floor((t_ms % 60000.0)/1000.0);

        //set text (if seconds are less than 10 we must add zero 7:8 -> 7:08)
        if (secs < 10)
            this.duration_textview.setText(mins + ":0" + secs);
        else
            this.duration_textview.setText(mins + ":" +  secs);
    }

    @Override
    public void onClick(View v) { }
}
