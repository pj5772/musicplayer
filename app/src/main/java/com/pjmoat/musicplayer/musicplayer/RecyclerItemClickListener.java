    package com.pjmoat.musicplayer.musicplayer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;

import org.w3c.dom.Text;

import java.util.Arrays;


public class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
    private OnItemClickListener mListener;

    private final String TAG = "com.pjmoat.musicplayer";

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);

        public void onLongItemClick(View view, int position);
    }

    GestureDetector mGestureDetector;

    public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                Log.d(TAG,"<RecyclerItemClickListener.RecyclerItemClickListener.onSingleTapUp>");
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                Log.d(TAG,"<RecyclerItemClickListener.RecyclerItemClickListener.onLongPress>");

                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && mListener != null) {
                    mListener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                }
            }
        });
    }

    @Override public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        Log.d(TAG,"<RecyclerItemClickListener.onInterceptTouchEvent>");

        //get child under click event in recycler view
        View childView = view.findChildViewUnder(e.getX(), e.getY());

        // if user did click a childview(childView != null) of recyclerview, mListener is configured,
        // gesture detector handles the MotionEvent, and the motionevent does not occur inside a seekbar
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e) && !inSeekBar(view,e)) {

            //pass event to mListener
            mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
            return true;
        }
        return false;
    }

    @Override public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {}

    @Override
    public void onRequestDisallowInterceptTouchEvent (boolean disallowIntercept){}

    //determines whether motion event occurs in a seekbar of the passed recyclerview
    private boolean inSeekBar(RecyclerView recyclerView, MotionEvent e) {
        for (int i = 0; i < recyclerView.getChildCount(); i++) {
            //get viewholder for this row
            View rowView = recyclerView.findViewHolderForAdapterPosition(i).itemView;

            //get seekbar view
            View seekbar = rowView.findViewById(R.id.song_seekbar);

            //find position of seekbar
            int[] pos = new int[2];
            seekbar.getLocationOnScreen(pos);

            //get size
            int width  = seekbar.getWidth();
            int height = seekbar.getHeight();

            //determine whether user is clicking seekbar
            boolean in_x = (pos[0] < e.getX()) && (e.getX() < pos[0]+width);
            boolean in_y = (pos[1]-height < e.getY()) && (e.getY() < pos[1]);
            if (in_x && in_y)
                return true;

            //Log.d(TAG,"size: " + Arrays.toString(size) + ", pos: " + Arrays.toString(pos) + ", in: " + Arrays.toString(in) + ", click: " + Arrays.toString(click));
        }

        return false;
    }
}
