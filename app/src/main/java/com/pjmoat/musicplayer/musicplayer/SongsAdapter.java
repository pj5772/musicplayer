package com.pjmoat.musicplayer.musicplayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SongsAdapter extends RecyclerView.Adapter<SongHolder> {

    private final String TAG = "com.pjmoat.musicplayer";

    Context context;
    ArrayList<Song> songs;
    private static LayoutInflater inflater = null;

    private int itemResource;

    SeekbarViewProgressChangedListener mListener;
    interface SeekbarViewProgressChangedListener {
        void onSeekBarProgressChanged(int progress, boolean fromUser);
    }

    public SongsAdapter(Context context, int itemResource, ArrayList<Song> songs, SeekbarViewProgressChangedListener listener) {
        this.context = context;
        this.songs = songs;
        this.itemResource = itemResource;

        mListener = listener;
    }

    //number of element in recyclerview
    @Override
    public int getItemCount() {
        return songs.size();
    }

    @Override
    public SongHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate view
        View view = LayoutInflater.from(parent.getContext()).inflate(this.itemResource, parent, false);

        //handle propagating up when seekbar progress changes
        SongHolder songHolder = new SongHolder(this.context, view, new SongHolder.SeekbarProgressChangedListener() {
            @Override
            public void onSeekBarProgressChanged(int new_progress,boolean fromUser) {
                Log.d(TAG,"here1");

                //pass up that progress of seekbar changed
                mListener.onSeekBarProgressChanged(new_progress,fromUser);
            }
        });

        return songHolder;
    }

    @Override
    public void onBindViewHolder(SongHolder holder, int position) {
        //get song at this position
        Song song = this.songs.get(position);

        //bind song to holder
        holder.bindSong(song);
    }

}
