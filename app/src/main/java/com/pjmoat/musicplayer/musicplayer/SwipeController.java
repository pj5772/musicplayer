package com.pjmoat.musicplayer.musicplayer;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.Arrays;

import static android.support.v7.widget.helper.ItemTouchHelper.*;

enum ButtonsState {
    GONE,
    RIGHT_VISIBLE
}

public class SwipeController  extends Callback {
    boolean swipeBack;

    private ButtonsState buttonShowedState = ButtonsState.GONE;

    private final String TAG = "com.pjmoat.musicplayer";

    private boolean holding_seekbar = false;

    private OnSongButtonClickListener mListener;
    public interface OnSongButtonClickListener {
        public void onOpenSongButtonClicked();

        public void onSeekBarProgressChanged(int progress);
    }

    public SwipeController(OnSongButtonClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return makeMovementFlags(0, LEFT | RIGHT);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {}

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        if (swipeBack) {
            swipeBack = false;
            return 0;
        }
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    @Override
    public void onChildDraw(Canvas c,
                            RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder,
                            float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        Log.d(TAG,"<SwipeController.onChildDraw>");

        if (actionState == ACTION_STATE_SWIPE) {
            setTouchListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }

        //dont redraw if user is holding a seekbar
        if (holding_seekbar)
            return;

        //if this is not the currently swiped row, skip
        if (MusicState.getInstance().current_view_swiped_pos != -1 &&
                MusicState.getInstance().current_view_swiped_pos != viewHolder.getAdapterPosition())
            return;

        //draw listview element. If button is visible draw listview element buttonWidth to the left
        if (buttonShowedState == ButtonsState.RIGHT_VISIBLE && viewHolder.getAdapterPosition() == MusicState.getInstance().current_view_swiped_pos)
            super.onChildDraw(c, recyclerView, viewHolder, -1 * MusicState.getInstance().view_song_button_width, dY, actionState, isCurrentlyActive);
        else if (dX < 0)
            super.onChildDraw(c, recyclerView, viewHolder, Math.max(dX,-1 * MusicState.getInstance().view_song_button_width), dY, actionState, isCurrentlyActive);
        else
            super.onChildDraw(c, recyclerView, viewHolder, 0, dY, actionState, isCurrentlyActive);
    }

    // SwipeController.java
    private void setTouchListener(final Canvas c,
                                  final RecyclerView recyclerView,
                                  final RecyclerView.ViewHolder viewHolder,
                                  final float dX, final float dY,
                                  final int actionState, final boolean isCurrentlyActive) {
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG,"<SwipeController.setTouchListener.onTouch>");

                //find progress in seekbar touched by user, -1 if user is not touching any seekbars
                int progress_in_seekbar = seekbarTouchProgress(recyclerView,event);

                if (progress_in_seekbar == -1)
                    holding_seekbar = false;
                else
                    holding_seekbar = true;

                //is onTouch being called in currently selected song
                boolean is_selected_song = MusicState.getInstance().songs.get(viewHolder.getAdapterPosition()).id == MusicState.getInstance().current_song_id;

                //if we are touching the seekbar in the selected song
                if (progress_in_seekbar != -1 && is_selected_song) {

                    //update progress in song to what user is currently selecting
                    mListener.onSeekBarProgressChanged(progress_in_seekbar);

                    //dont make any drawing changes to this row
                    return false;
                }

                swipeBack = event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_UP;

                //exit if this is not swiped element
                if (MusicState.getInstance().current_view_swiped_pos != -1 &&
                        viewHolder.getAdapterPosition() != MusicState.getInstance().current_view_swiped_pos)
                    return false;

                //is user clicks button to view song
                if (event.getX() > Resources.getSystem().getDisplayMetrics().widthPixels - MusicState.getInstance().view_song_button_width &&
                        viewHolder.getAdapterPosition() == MusicState.getInstance().current_view_swiped_pos) {
                    mListener.onOpenSongButtonClicked();
                    SwipeController.super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }

                if (swipeBack) {
                    if (dX < -MusicState.getInstance().view_song_button_width) {
                        //set button to visible
                        buttonShowedState = ButtonsState.RIGHT_VISIBLE;

                        //set hash of current swiped view
                        MusicState.getInstance().current_view_swiped_pos = viewHolder.getLayoutPosition();
                    }
                    else {
                        //remove button since we are not swiped
                        buttonShowedState = ButtonsState.GONE;

                        //if this is the currently swiped id, reset it
                        MusicState.getInstance().current_view_swiped_pos = -1;
                    }


                    if (buttonShowedState != ButtonsState.GONE) {
                        setTouchDownListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        setItemsClickable(recyclerView, false);
                    }
                }

                return false;
            }
        });
    }

    // SwipeController.java
    private void setTouchDownListener(final Canvas c,
                                      final RecyclerView recyclerView,
                                      final RecyclerView.ViewHolder viewHolder,
                                      final float dX, final float dY,
                                      final int actionState, final boolean isCurrentlyActive) {
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG,"<SwipeController.setTouchDownListener.onTouch>");

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    setTouchUpListener(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
                return false;
            }
        });
    }

    private void setTouchUpListener(final Canvas c,
                                    final RecyclerView recyclerView,
                                    final RecyclerView.ViewHolder viewHolder,
                                    final float dX, final float dY,
                                    final int actionState, final boolean isCurrentlyActive) {
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d(TAG,"<SwipeController.setTouchUpListener.onTouch>");

                //if this is not currently swiped position exit
                if (viewHolder.getAdapterPosition() != MusicState.getInstance().current_view_swiped_pos)
                    return false;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    SwipeController.super.onChildDraw(c, recyclerView, viewHolder, 0F, dY, actionState, isCurrentlyActive);
                    recyclerView.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return false;
                        }
                    });
                    setItemsClickable(recyclerView, true);
                    swipeBack = false;
                    buttonShowedState = ButtonsState.GONE;
                }
                return false;
            }
        });
    }

    private void setItemsClickable(RecyclerView recyclerView,
                                   boolean isClickable) {
        Log.d(TAG,"<setItemsClickable>");

        for (int i = 0; i < recyclerView.getChildCount(); ++i) {
            recyclerView.getChildAt(i).setClickable(isClickable);
        }
    }

    //determines whether motion event occurs in a seekbar of the passed recyclerview
    private int seekbarTouchProgress(RecyclerView recyclerView, MotionEvent e) {
        for (int i = 0; i < recyclerView.getChildCount(); i++) {
            //get viewholder for this row
            View rowView = recyclerView.findViewHolderForAdapterPosition(i).itemView;

            //get seekbar view
            View seekbar = rowView.findViewById(R.id.song_seekbar);

            //find position of seekbar
            int[] pos = new int[2];
            seekbar.getLocationOnScreen(pos);

            //get size
            int width  = seekbar.getWidth();
            int height = seekbar.getHeight();

            //determine whether user is clicking seekbar
            boolean in_x = (pos[0] < e.getX()) && (e.getX() < pos[0]+width);
            boolean in_y = (pos[1]-height < e.getY()) && (e.getY() < pos[1]);
            boolean in = in_x && in_y;

            //if user is touching seekbar
            if (in)
                return (int) (100.00*(  ((double)(e.getX() - pos[0])) / ((double)width))); //return progress
        }

        //return -1, we are not touching any seekbar
        return -1;
    }
}
